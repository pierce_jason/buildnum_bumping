#!/bin/sh

# Copyright 2007-2010  Robby Workman, Northport, Alabama, USA
# Copyright 2007-2010  Patrick Volkerding, Sebeka, MN, USA
# All rights reserved.
#
# Redistribution and use of this script, with or without modification, is
# permitted provided that the following conditions are met:
#
# 1. Redistributions of this script must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
# WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
# EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
# OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
# ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

NAME="dbus"
VERSION=${VERSION:-1.4.20}
BUILD=${BUILD:-1vl70}
LINK=${LINK:-"http://dbus.freedesktop.org/releases/$NAME/$NAME-$VERSION.tar.gz"}


if [ "$NORUN" != 1 ]; then

NUMJOBS=${NUMJOBS:-" -j7 "}

# Automatically determine the architecture we're building on:
if [ -z "$ARCH" ]; then
  case "$( uname -m )" in
    i?86) export ARCH=i586 ;;
    arm*) export ARCH=arm ;;
    # Unless $ARCH is already set, use uname -m for all other archs:
       *) export ARCH=$( uname -m ) ;;
  esac
fi

CWD=$(pwd)
cd ../
RELEASEDIR=$(pwd)
cd $CWD
mkdir -p $RELEASEDIR/tmp
TMP=$RELEASEDIR/tmp

PKG=$TMP/package-$PKGNAM

if [ "$ARCH" = "i586" ]; then
  SLKCFLAGS="-O2 -march=i586 -mtune=i686"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "s390" ]; then
  SLKCFLAGS="-O2"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "x86_64" ]; then
  SLKCFLAGS="-O2 -fPIC"
  LIBDIRSUFFIX="64"
else
  SLKCFLAGS="-O2"
  LIBDIRSUFFIX=""
fi
#get the source..........
for SRC in $(echo $LINK);do
if [ ! -f $CWD/$(basename $SRC) ]
then
	wget --no-check-certificate -c $SRC
fi
done
rm -rf $PKG
mkdir -p $TMP $PKG
cd $TMP
rm -rf $NAME-$VERSION
tar xvf $CWD/$NAME-$VERSION.tar.gz || exit 1
cd $NAME-$VERSION || exit 1
chown -R root:root .
find . \
 \( -perm 777 -o -perm 775 -o -perm 711 -o -perm 555 -o -perm 511 \) \
 -exec chmod 755 {} \; -o \
 \( -perm 666 -o -perm 664 -o -perm 600 -o -perm 444 -o -perm 440 -o -perm 400 \) \
 -exec chmod 644 {} \;

zcat $CWD/dbus-1.4.x-allow_root_globally.diff.gz | patch -p1 --verbose || exit 1

CFLAGS="$SLKCFLAGS" \
CXXFLAGS="$SLKCFLAGS" \
./configure \
  --prefix=/usr \
  --libdir=/usr/lib${LIBDIRSUFFIX} \
  --sysconfdir=/etc \
  --localstatedir=/var \
  --mandir=/usr/man \
  --infodir=/usr/info \
  --docdir=/usr/doc/$PKGNAM-$VERSION \
  --enable-shared=yes \
  --enable-static=no \
  --enable-inotify \
  --with-system-pid-file=/var/run/dbus/dbus.pid \
  --with-system-socket=/var/run/dbus/system_bus_socket \
  --with-init-scripts=slackware \
  --build=$ARCH-slackware-linux

make $NUMJOBS || make || exit 1
make install DESTDIR=$PKG

find $PKG | xargs file | grep -e "executable" -e "shared object" \
  | grep ELF | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null

find $PKG/usr/man -type f -exec gzip -9 {} \;
for i in $( find $PKG/usr/man -type l ) ; do ln -s $( readlink $i ).gz $i.gz ; rm $i ; done

# Install a custom init script for dbus - the included one is not good for us
rm $PKG/etc/rc.d/*
zcat $CWD/rc.messagebus.gz > $PKG/etc/rc.d/rc.messagebus.new
chmod 0755 $PKG/etc/rc.d/rc.messagebus.new

# Fix some directory ownership
chown messagebus $PKG/var/lib/dbus

# Add documentation
mkdir -p $PKG/usr/doc/$NAME-$VERSION
cp -a \
  AUTHORS COPYING* HACKING INSTALL NEWS README* doc/*.{txt,html,dtd} \
  $PKG/usr/doc/$NAME-$VERSION
find $PKG/usr/doc/$NAME-$VERSION -type f -exec chmod 0644 {} \;

mkdir -p $PKG/install
cat $CWD/slack-desc > $PKG/install/slack-desc
zcat $CWD/doinst.sh.gz > $PKG/install/doinst.sh

cd $PKG
requiredbuilder -v -y -s $RELEASEDIR $PKG
makepkg -c n -l y $RELEASEDIR/$NAME-$VERSION-$ARCH-$BUILD.txz

rm -rf $TMP
fi
